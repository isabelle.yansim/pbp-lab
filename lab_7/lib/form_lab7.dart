import 'package:flutter/material.dart';


class FormScreen extends StatefulWidget {
  const FormScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState(){
    return FormScreenState();
  }
}

class FormScreenState extends State<FormScreen> {
  final _formKey = GlobalKey<FormState>();
  String? _author = " ";
  String? _title = " ";
  String? _body = " ";

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Rumah Harapan🏠"),
      ),

        body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                TextFormField(
                decoration: InputDecoration(
                  hintText: "Nama penulis",
                  labelText: 'Author',
                  icon: const Icon(Icons.people),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    setState(() {
                      _author = 'Kosong';
                    });
                    return 'Nama penulis tidak boleh kosong';
                  }
                  setState(() {
                    _author = value;
                  });
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.only(top: 20),),
              TextFormField(
                maxLines: 3,
                decoration: InputDecoration(
                  hintText: "Judul Artikel",
                  labelText: 'Title',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    setState(() {
                      _title = 'Kosong';
                    });
                    return 'Judul artikel tidak boleh kosong';
                  }
                  setState(() {
                    _title = value;
                  });
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.only(top: 20),),
              TextFormField(
                maxLines: 100,
                decoration: InputDecoration(
                  hintText: 'Isi Artikel',
                  labelText: 'Body',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    setState(() {
                      _body = 'Kosong';
                    });
                    return 'Isi artikel tidak boleh kosong';
                  }
                  setState(() {
                    _body = value;
                  });
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.only(top: 20),),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Tanggal publikasi',
                  labelText: 'Date Published',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
                validator: (value) {
                  if (value == null) {
                    return 'Tanggal publikasi tidak boleh kosong';
                  }
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.only(top: 20),),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {}
                },
                child: const Text(
                  "Tambahkan",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

