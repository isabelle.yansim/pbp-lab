import 'package:flutter/material.dart';


class FormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState(){
    return FormScreenState();
  }
}

class FormScreenState extends State<FormScreen> {
  
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Rumah Harapan🏠"),
      ),

      body: Container(
        padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  hintText: "Nama pengarang",
                  labelText: 'Author',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 20),),
              TextField(
                maxLines: 3,
                decoration: InputDecoration(
                  hintText: "Judul Artikel",
                  labelText: 'Title',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 20),),
              TextField(
                maxLines: 100,
                decoration: InputDecoration(
                  hintText: 'Isi Artikel',
                  labelText: 'Body',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              Padding(padding: EdgeInsets.only(top: 20),),
              TextField(
                decoration: InputDecoration(
                  hintText: 'Tanggal publikasi',
                  labelText: 'Date Published',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
            ],
          ),
      ),
    );
  }


}