import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(new MyApp(
  ));

}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: const Color(0xffADE8F4),
        appBar: AppBar(
          title: const Text("Rumah Harapan🏠"),
        ),
      
        body: ListView(
          padding: EdgeInsets.all(24),
          children: [
            const Text(
              "Artikel",
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 32,
                color: Color(0xff59A5D8),
              ), 
            ),
            SizedBox(height: 8),
            const Text(
              "Kami menyediakan media berupa artikel, yaitu karangan faktual yang dapat memberikan informasi-informasi menarik seputar topik-topik mengenai Covid-19 terhadap pembaca pada laman ini.",
              textAlign: TextAlign.left,
              style: TextStyle(
                height: 1.4,
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 30),),
            ElevatedButton(
              child: Text('Tambah Artikel' ),
              onPressed: () {},
            ),
            Padding(padding: EdgeInsets.only(top: 30),),
            const Text(
              "Daftar Artikel",
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 25,
                color: Color(0xff59A5D8),
              ), 
            ),
            Padding(padding: EdgeInsets.only(top: 20),),
            const CardWidget(),
          ],
        ),
      ),
    );
  }
}

class CardWidget extends StatelessWidget{
  const CardWidget({Key? key}) : super(key: key);

  @override 
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Card(
          child: Column(
            children: [
              InkWell(
                splashColor: Colors.blue.withAlpha(30),
                onTap: () {
                },
                child: const SizedBox(
                  width: 500,
                  height: 100,
                  child: 
                    Text(
                      ' anonymous | Nov 26, 2021 \n Title \n Description',
                      textAlign: TextAlign.left,
                      
                    ),
                    
                ),
              ),
              ElevatedButton(
                child: const Text('Lihat Detail' ),
                onPressed: () {},
              ),
              Padding(padding: EdgeInsets.only(top: 15),),
            ],
          ),
          
        ),
      ),
    );
  }
}

